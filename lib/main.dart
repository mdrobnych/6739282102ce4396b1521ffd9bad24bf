import 'package:flutter/material.dart';
import 'package:rocket_guide/app/app.dart';
import 'package:rocket_guide/backend/backend.dart';
import 'package:firebase_core/firebase_core.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  
  final backend = Backend('https://api.spacexdata.com/v4');

  runApp(
    RocketGuideApp(
      backend: backend,
    ),
  );
}
