import 'package:flutter/material.dart';
import 'package:rocket_guide/backend/backend.dart';
import 'package:provider/provider.dart';
import 'package:ant_icons/ant_icons.dart';

class RocketListTile extends StatelessWidget {
  const RocketListTile({
    Key key,
    @required this.rocket,
    @required this.onTap,
  })  : assert(rocket != null),
        super(key: key);
  final Rocket rocket;
  final VoidCallback onTap;
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<String>>(
        stream: context.read<Backend>().favoritedRockets,
        builder: (context, snapshot) {
          final hasFavorited = snapshot.hasData && snapshot.data.contains(rocket.id);
          return ListTile(
            isThreeLine: true,
            onTap: onTap,
            leading: rocket.flickrImages.isEmpty
                ? null
                : Hero(
                    tag: 'hero-${rocket.id}-image',
                    child: Material(
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(8.0),
                      child: AspectRatio(
                        aspectRatio: 3 / 2,
                        child: Image.network(
                          rocket.flickrImages.first,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
            title: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Hero(
                  tag: 'hero-${rocket.id}-name',
                  child: Text(rocket.name),
                ),
                if (hasFavorited) ...const [
                  SizedBox(width: 4.0),
                  Icon(
                    AntIcons.heart,
                    color: Colors.redAccent,
                    size: 16.0,
                  ),
                ],
              ],
            ),
            subtitle: Text(
              rocket.description,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
            trailing: const Icon(Icons.chevron_right_sharp),
          );
        });
  }
}
